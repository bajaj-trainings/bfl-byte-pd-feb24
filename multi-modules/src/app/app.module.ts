import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RootComponent } from './components/root/root.component';
import { MoneModule } from './mone/mone.module';
import { MtwoModule } from './mtwo/mtwo.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    RootComponent
  ],
  imports: [
    MoneModule,
    MtwoModule,
    SharedModule,
    BrowserModule
  ],
  providers: [],
  bootstrap: [
    RootComponent
  ]
})
export class AppModule { }
