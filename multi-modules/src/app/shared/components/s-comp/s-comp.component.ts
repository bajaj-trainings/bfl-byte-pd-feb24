import { Component } from '@angular/core';

@Component({
  selector: 's-comp',
  template: `
    <h3 class="text-warning">Shared Component</h3>
  `,
  styles: [
  ]
})
export class SCompComponent {

}
