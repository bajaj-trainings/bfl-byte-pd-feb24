import { Component } from '@angular/core';

@Component({
  selector: 'hello',
  template: `
        <h3 class="text-primary">Hola World!</h3>
  `,
  styles: [
  ]
})
export class HelloComponent {

}
