import { Component } from '@angular/core';

@Component({
  selector: 'comp-two',
  template: `
    <h1 class="text-primary">
      Hello from Component Two Module Two
    </h1>
    <hello></hello>
    <s-comp></s-comp>
  `,
  styles: [
  ]
})
export class CompTwoComponent {

}
