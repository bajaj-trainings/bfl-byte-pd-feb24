import { Component } from '@angular/core';

@Component({
  selector: 'hello',
  template: `
    <h3 class="text-success">Hello World!</h3>
  `,
  styles: [
  ]
})
export class HelloComponent {

}
