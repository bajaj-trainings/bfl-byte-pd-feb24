import { Component } from '@angular/core';

@Component({
  selector: 'comp-one',
  template: `
    <h1 class="text-success">
      Hello from Component One Module One
    </h1>
    <hello></hello>
    <s-comp></s-comp>
  `,
  styles: [
  ]
})
export class CompOneComponent {

}
