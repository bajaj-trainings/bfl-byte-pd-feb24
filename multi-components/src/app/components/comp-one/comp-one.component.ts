import { Component } from '@angular/core';

@Component({
  selector: 'comp-one',
  template: `
    <h1 class="text-success">Hello from Component One</h1>
  `
})
export class CompOneComponent { }
