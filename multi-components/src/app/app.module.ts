// // ----------------------------------------------------------------- Default Bootstraping

// import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
// import { CompOneComponent } from './components/comp-one/comp-one.component';
// import { CompTwoComponent } from './components/comp-two/comp-two.component';

// @NgModule({
//   declarations: [
//     CompOneComponent,
//     CompTwoComponent
//   ],
//   imports: [
//     BrowserModule
//   ],
//   providers: [],
//   bootstrap: [
//     CompOneComponent,
//     CompTwoComponent
//   ]
// })
// export class AppModule { }

// ----------------------------------------------------------------- Custom Bootstraping

// import { ApplicationRef, DoBootstrap, NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
// import { CompOneComponent } from './components/comp-one/comp-one.component';
// import { CompTwoComponent } from './components/comp-two/comp-two.component';

// @NgModule({
//   declarations: [
//     CompOneComponent,
//     CompTwoComponent
//   ],
//   imports: [
//     BrowserModule
//   ]
// })
// export class AppModule implements DoBootstrap {
//   ngDoBootstrap(appRef: ApplicationRef): void {
//     let condition = true;
//     const appDiv = document.querySelector('#app');

//     if (condition) {
//       const compOne = document.createElement('comp-one');
//       appDiv?.appendChild(compOne);
//       appRef.bootstrap(CompOneComponent);
//     } else {
//       const compTwo = document.createElement('comp-two');
//       appDiv?.appendChild(compTwo);
//       appRef.bootstrap(CompTwoComponent);
//     }
//   }
// }

// ----------------------------------------------------------------- Using Root
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CompOneComponent } from './components/comp-one/comp-one.component';
import { CompTwoComponent } from './components/comp-two/comp-two.component';
import { RootComponent } from './components/root/root.component';

@NgModule({
  declarations: [
    CompOneComponent,
    CompTwoComponent,
    RootComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [
   RootComponent
  ]
})
export class AppModule { }
