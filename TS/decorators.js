"use strict";
// class Calculator {
//     add(x: number, y: number) {
//         log(`add method called with arguments as ${x} and ${y}`);
//         return x + y;
//     }
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//     sub(x: number, y: number) {
//         log(`sub method called with arguments as ${x} and ${y}`);
//         return x - y;
//     }
// }
// var c = new Calculator();
// console.log(c.add(2, 3));
// console.log(c.sub(24, 35));
// // I want to log the arguments passed to the method, when we call the methods
// // Where should we write the Code for logging?
// function log(message: string) {
//     console.log(message);
// }
class Calculator {
    add(x, y) {
        return x + y;
    }
    sub(x, y) {
        return x - y;
    }
}
__decorate([
    log,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number]),
    __metadata("design:returntype", void 0)
], Calculator.prototype, "add", null);
__decorate([
    log,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number]),
    __metadata("design:returntype", void 0)
], Calculator.prototype, "sub", null);
var c = new Calculator();
console.log(c.add(2, 3));
console.log(c.sub(24, 35));
// Method Decorator
function log(target, propertyKey, descriptor) {
    const originalFn = descriptor.value;
    descriptor.value = function (...args) {
        console.log(`${propertyKey} is called with arguments as ${args}`);
        return originalFn(...args);
    };
    return descriptor;
}
