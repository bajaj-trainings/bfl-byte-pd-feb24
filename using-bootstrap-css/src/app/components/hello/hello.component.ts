import { Component } from '@angular/core';

@Component({
  selector: 'app-hello',
  template: `
    <div class="container-fluid text-center">
        <h1 class="red">Hello</h1>
        <h1 class="text-primary">Hello World!</h1>
        <h1 class="text-success">Hello World!</h1>
        <h1 class="text-secondary">Hello World!</h1>
        <h1 class="text-warning">Hello World!</h1>
        <h1 class="text-info">
          Home: <span class="bi bi-house"></span>
        </h1>
        <h1 class="text-info">
          Eight: <span class="bi bi-8-square-fill"></span>
        </h1>
    </div>
  `,
  styles: [
  ]
})
export class HelloComponent {

}
