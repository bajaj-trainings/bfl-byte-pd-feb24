import { NgModule } from '@angular/core';
import { BflLibComponent } from './bfl-lib.component';



@NgModule({
  declarations: [
    BflLibComponent
  ],
  imports: [
  ],
  exports: [
    BflLibComponent
  ]
})
export class BflLibModule { }
